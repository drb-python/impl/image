import rasterio
from drb.drivers.file import DrbFileNode

from drb.drivers.image import DrbImageBaseNode
from drb.drivers.image.simple_node import DrbImageNodesValueNames


image_jpeg = "files/sample_640×426.jpeg"

base_node = DrbFileNode(image_jpeg)
image_node = DrbImageBaseNode(base_node)

# get the name of the file
image_node.name

# Get the full path of the file
image_node.path.original_path

# Access to the image node.

# Get the width and height of this image
image_node[DrbImageNodesValueNames.HEIGHT.value]
image_node[DrbImageNodesValueNames.WIDTH.value]

# Retrieve the data set of the image
impl = image_node.get_impl(rasterio.DatasetReader)
read = impl.read()

image_node.close()
impl.close()
