.. _example:

Examples
=========

Open and read an image
-----------------------
.. literalinclude:: example/open.py
    :language: python
