===================
Data Request Broker
===================
---------------------------------
IMAGE driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-image/month
    :target: https://pepy.tech/project/drb-driver-image
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-image.svg
    :target: https://pypi.org/project/drb-driver-image/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-image.svg
    :target: https://pypi.org/project/drb-driver-image/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-image.svg
    :target: https://pypi.org/project/drb-driver-image/
    :alt: Python Version Support Badge

-------------------

This drb-driver-image module implements images data formats to be accessed with DRB data model.
It is able to navigates among the images contents and accessing the image data.


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

