.. _install:

Installation of image implementation
====================================
To include this module into your project, the drb-driver-image module shall be referenced into requirement.txt file,
or the following pip line can be run:
.. code-block::

    pip install drb-driver-image
