from drb.exceptions.core import DrbException, DrbFactoryException


class DrbImageNodeException(DrbException):
    pass


class DrbImageNodeFactoryException(DrbFactoryException):
    pass
